package com.example.smails.GenericRepository;


import java.util.Optional;

/**
 * @author user
 */
public interface GenericRepository<T, ID>
{

    Iterable<T> findAll();

    Optional<T>  findById(ID id);

    <S extends T> S save(S s);

    void deleteById(ID id);

}
