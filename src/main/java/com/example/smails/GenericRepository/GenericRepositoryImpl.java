package com.example.smails.GenericRepository;

import com.example.smails.GenericRepository.mapper.BasicMapper;
import com.example.smails.GenericRepository.mapper.MapMapper;
import com.example.smails.GenericRepository.mapper.Mapper;
import com.example.smails.controller.DBCPDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.sql.*;
import java.util.*;

/**
 * @author user
 */
public class GenericRepositoryImpl<T, ID> implements GenericRepository<T, ID> {
    private static final Logger log = LogManager.getLogger(GenericRepositoryImpl.class);

    protected Class<T> clazz;
    protected String table;
    protected String SQLSelect;
    //    protected String SQLInsert;
    //    protected String SQLUpdate;
    protected String SQLDelete;

    protected Mapper<T> defaultMapper;

    protected void init(Class<T> clazz) {
        init(clazz, clazz.getSimpleName().toLowerCase(Locale.ROOT));
    }

    protected void init(Class<T> clazz, String table) {

        this.clazz = clazz;

        this.table = table;

        //default mapper
        defaultMapper = new BasicMapper<>(clazz);

        SQLSelect = "SELECT * FROM " + table;
//        SQLInsert = "INSERT INTO " + table + " (" + defaultMapper.getColumnNames() + ") VALUES ("+defaultMapper.getColumnMarks() +")";
//        SQLUpdate = "UPDATE " + table + " SET " + defaultMapper.getColumnNamesForUpdate() + " WHERE id=?";
        SQLDelete = "DELETE FROM " + table;

    }

    private String getSQL(Mapper mapper, boolean update) {
        return (update ?
                "UPDATE " + table + " SET " + mapper.getColumnNamesForUpdate() + " WHERE id=?" :
                "INSERT INTO " + table + " (" + mapper.getColumnNames() + ") VALUES (" + mapper.getColumnMarks() + ")");
    }

    @Override
    public List<T> findAll() {
        return findAll(defaultMapper);
    }

    public List<T> findAll(Class<T> clazz) {
        return findAll(getMapper(clazz));
    }

    public List<T> findAll(Mapper<T> mapper) {
        mapper.resetCaches();
        List<T> result = new ArrayList<>();
        try (Connection connection = DBCPDataSource.getConnection();
             PreparedStatement ps = connection.prepareCall(SQLSelect)) {
            log.info("@@@@SQL: " + ps);
            ResultSet rs = ps.executeQuery();
            while (rs.next())
                result.add(mapper.map(rs));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    public List<Map<String, Object>> findAll(String sql) {
        List<Map<String, Object>> result = new ArrayList<>();
        try (Connection connection = DBCPDataSource.getConnection();
             PreparedStatement ps = connection.prepareCall(sql)) {
            log.info("@@@@SQL: " + ps);
            ResultSet rs = ps.executeQuery();
            MapMapper mapMapper = new MapMapper();
            while (rs.next()) {
                result.add(mapMapper.map(rs));
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    public List<Map<String, Object>> findAll(String sql, Object... parameters) {
        List<Map<String, Object>> result = new ArrayList<>();
        try (Connection connection = DBCPDataSource.getConnection();
             PreparedStatement ps = connection.prepareCall(sql)) {
            int i = 1;
            for (Object parameter : parameters)
                ps.setObject(i++, parameter);

            log.info("@@@@SQL: " + ps);
            ResultSet rs = ps.executeQuery();
            MapMapper mapMapper = new MapMapper();
            while (rs.next()) {
                result.add(mapMapper.map(rs));
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    @Override
    public Optional<T> findById(ID id) {
        return findBy(defaultMapper, "id", id);
    }

    public <S> Optional<S> findById(Class<S> clazz, ID id) {
        return findBy(getMapper(clazz), "id", id);
    }

    public <S> Optional<S> findById(Mapper<S> mapper, ID id) {
        return findBy(mapper, "id", id);
    }

    public Optional<T> findBy(String field, Object value) {
        return findBy(defaultMapper, field, value);
    }


    public Optional<T> findBy(String field1, Object value1, String field2, Object value2) {
        return findBy(defaultMapper, field1, value1, field2, value2);
    }

    public <S> Optional<S> findBy(Class<S> clazz, String field, Object value) {
        return findBy(getMapper(clazz), field, value);
    }

    public <S> Optional<S> findBy(Class<S> clazz, String field1, Object value1, String field2, Object value2) {
        return findBy(getMapper(clazz), field1, value1, field2, value2);
    }

    public <S> Optional<S> findBy(Mapper<S> mapper, String field, Object value) {

        mapper.resetCaches();
        Optional<S> result = Optional.empty();
        String sql = SQLSelect + " WHERE " + field + " = ?";
        try (Connection connection = DBCPDataSource.getConnection();
             PreparedStatement ps = connection.prepareCall(sql)) {
            ps.setObject(1, value);
            log.info("@@@@SQL: " + ps);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                result = Optional.of(mapper.map(rs));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    public <S> Optional<S> findBy(Mapper<S> mapper, String field1, Object value1, String field2, Object value2) {

        mapper.resetCaches();
        Optional<S> result = Optional.empty();
        String sql = SQLSelect + " WHERE " + field1 + " = ? AND " + field2 + " = ?";
        try (Connection connection = DBCPDataSource.getConnection();
             PreparedStatement ps = connection.prepareCall(sql)) {
            ps.setObject(1, value1);
            ps.setObject(2, value2);
            log.info("@@@@SQL: " + ps);
            ResultSet rs = ps.executeQuery();
            if (rs.next())
                result = Optional.of(mapper.map(rs));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }


    @Override
    public <S extends T> S save(S object) {
        return (S) save(defaultMapper, object);
    }

    public <S> S save(Class<S> clazz, S object) {
        return save(getMapper(clazz), object);
    }

    public <S> S save(Mapper<S> mapper, S object) {
        Object id = mapper.getId(object);
        boolean update = (id != null && !id.equals(0));
        String sql = getSQL(mapper, update);
        try (Connection con = DBCPDataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            int n = mapper.unmap(ps, object, update);
            if (update) ps.setObject(n + 1, id);
            log.info("@@@@SQL: " + ps);
            ps.execute();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                rs.next();
                if (!update) mapper.setId(object, rs);
                return object;
            }
        } catch (SQLException ex) {
            log.error("Failed to insert/update table {} value {}", table, object);
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void deleteById(ID id) {
        deleteBy("id", id);
    }

    public int deleteBy(String column, Object value) {
        String sql = SQLDelete + " WHERE " + column + " = ?";

        try (Connection con = DBCPDataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setObject(1, value);
            log.info("@@@@SQL: " + ps);
            return ps.executeUpdate();
        } catch (SQLException ex) {
            log.error("Failed to delete from {} by column {} and value {}", table, column, value);
            throw new RuntimeException(ex);
        }
    }

    public <S> Mapper<S> getMapper(Class<S> clazz) {
        return new BasicMapper<>(clazz);
    }
}
