package com.example.smails.GenericRepository.mapper;

import com.example.smails.GenericRepository.GenericRepository;
import com.example.smails.GenericRepository.mapper.getter.Getter;
import com.example.smails.GenericRepository.mapper.getter.SimpleGetter;
import com.example.smails.GenericRepository.mapper.setter.*;
import com.example.smails.controller.BeanHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.example.smails.GenericRepository.mapper.Utils.getColumnLabel;
import static com.example.smails.GenericRepository.mapper.Utils.setterCanBeGenerated;

/**
 * @author user
 */
public class BasicMapper<T> implements Mapper<T> {
    private static final Logger log = LogManager.getLogger(BasicMapper.class);

    protected Class<T> clazz = null;

    protected Map<String, Setter> setters = new HashMap<>();
    protected Map<String, Getter> getters = new LinkedHashMap<>();
    protected Map<String, Getter> gettersForUpdate = new LinkedHashMap<>();
    protected Getter idGetter;
    protected Setter idSetter;

    protected Map<Class, Map<Object, Object>> caches = new HashMap<>();

    public BasicMapper(Class<T> clazz) {

        this.clazz = clazz;

        //default mapper
        Field[] columns = clazz.getDeclaredFields();
        try {
            for (Field f : columns) {
                Class<?> type = f.getType();
                Method setterMethod = clazz.getDeclaredMethod(Utils.getSetterName(f.getName()), type);
                Method getterMethod = clazz.getDeclaredMethod(Utils.getGetterName(f.getName()));
                String columnLabel = getColumnLabel(f);
                Getter<T> getter;
                Setter<T> setter;
                getter = new SimpleGetter<T>(getterMethod, columnLabel);
                if (setterCanBeGenerated(type)) {
                    Class sqlType = Utils.getSqlType(type);
                    if (sqlType == type) {
                        setter = new SimpleSetter<T>(setterMethod, columnLabel);
                    } else {
                        setter = new CastSetter<T>(setterMethod, columnLabel, sqlType, type);
                    }
                } else {
                    // maybe we have a Repository of this type...
                    String repoClassName = type.getName() + "Repository";
                    if (!BeanHolder.contains(repoClassName)) continue;
                    GenericRepository repo = (GenericRepository) BeanHolder.getBean(Class.forName(repoClassName));
                    Map<Object, Object> cache = caches.computeIfAbsent(type, k -> new HashMap<>());
                    setter = new EntitySetter(setterMethod, columnLabel, repo, cache);
                }
                setters.put(columnLabel, setter);
                if ("id".equals(columnLabel)) {
                    idGetter = getter;
                    idSetter = setter;
                } else {
                    //TODO annotation @Generated(GenerationTime.INSERT)
                    if (!"number".equals(columnLabel)) {
                        getters.put(columnLabel, getter);
                        //TODO annotation @Column(updatable = false)
                        if (!"acc_id".equals(columnLabel) && !"created".equals(columnLabel))
                            gettersForUpdate.put(columnLabel, getter);
                    }
                }
            }
        } catch (Exception e) {
            log.error("GenericRepositoryImpl init failure", e);
        }
    }

    public void setSetter(String column, Setter<T> setter) {
        setters.put(column, setter);
    }

    public void removeGetter(String column) {
        setters.remove(column);
    }

    public void resetCaches() {
        for (Map cache : caches.values()) cache.clear();
    }

    @Override
    public int unmap(PreparedStatement ps, T object, boolean update) {
        int i = 0;
        try {
            for (Getter getter : (update ? gettersForUpdate : getters).values())
                ps.setObject(++i, getter.get(object));
        } catch (Exception e) {
            log.error("Unmap failure", e);
        }
        return i;
    }

    @Override
    public Object getId(T object) {
        try {
            return idGetter.get(object);
        } catch (Exception e) {
            log.error("getId failure", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setId(T object, ResultSet rs) {
        try {
            idSetter.set(object, rs);
        } catch (Exception e) {
            log.error("setId failure", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getColumnMarks() {
        StringBuffer sb = new StringBuffer("?");
        for (int i = getters.keySet().size(); i > 1; i--)
            sb.append(",?");
        return sb.toString();
    }

    @Override
    public String getColumnNames() {
        return getters.keySet().stream().collect(Collectors.joining(","));
    }

    @Override
    public String getColumnNamesForUpdate() {
        return gettersForUpdate.keySet().stream().collect(Collectors.joining("=?,", "", "=?"));
    }

    @Override
    public T map(ResultSet rs) {
        T result = null;
        try {
            result = (T) clazz.newInstance();
            for (Setter setter : setters.values()) {
                setter.set(result, rs);
            }
        } catch (Exception e) {
            log.error("Instance creation failure", e);
        }
        return result;
    }
}
