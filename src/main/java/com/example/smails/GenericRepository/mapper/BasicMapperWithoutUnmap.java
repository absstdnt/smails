package com.example.smails.GenericRepository.mapper;

import com.example.smails.GenericRepository.GenericRepository;
import com.example.smails.GenericRepository.mapper.setter.CastSetter;
import com.example.smails.GenericRepository.mapper.setter.EntitySetter;
import com.example.smails.GenericRepository.mapper.setter.Setter;
import com.example.smails.GenericRepository.mapper.setter.SimpleSetter;
import com.example.smails.controller.BeanHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import static com.example.smails.GenericRepository.mapper.Utils.getColumnLabel;
import static com.example.smails.GenericRepository.mapper.Utils.setterCanBeGenerated;

/**
 * @author user
 */
public class BasicMapperWithoutUnmap<T> implements Mapper<T> {
    private static final Logger log = LogManager.getLogger(BasicMapperWithoutUnmap.class);

    protected Class<T> clazz = null;

    protected Map<String, Setter> setters = new HashMap<>();

    protected Map<Class, Map<Object, Object>> caches = new HashMap<>();

    public BasicMapperWithoutUnmap(Class<T> clazz) {

        this.clazz = clazz;

        //default mapper
        Field[] fields = clazz.getDeclaredFields();
        try {
            for (Field f : fields) {
                Class<?> type = f.getType();
                Method m = clazz.getDeclaredMethod(Utils.getSetterName(f.getName()), type);
                String columnLabel = getColumnLabel(f);
                if (setterCanBeGenerated(type)) {
                    Class sqlType = Utils.getSqlType(type);
                    if (sqlType == type) setters.put(columnLabel, new SimpleSetter(m, columnLabel));
                    else setters.put(columnLabel, new CastSetter(m, columnLabel, sqlType, type));
                    continue;
                }
                // maybe we have a Repository of this type...
                String repoClassName = type.getName() + "Repository";
                if (!BeanHolder.contains(repoClassName)) continue;

                GenericRepository repo = (GenericRepository) BeanHolder.getBean(Class.forName(repoClassName));

                Map<Object, Object> cache = caches.get(type);
                if (cache == null) {
                    cache = new HashMap<>();
                    caches.put(type, cache);
                }
                setters.put(columnLabel, new EntitySetter(m, columnLabel, repo, cache));
            }
        } catch (Exception e) {
            log.error("GenericRepositoryImpl init failure", e);
        }

    }

    public void setSetter(String field, Setter<T> setter) {
        setters.put(field, setter);
    }

    public void resetCaches() {
        for (Map cache: caches.values()) cache.clear();
    }

    @Override
    public T map(ResultSet rs) {
        T result = null;
        try {
            result = (T) clazz.newInstance();
            for (Setter setter : setters.values()) {
                setter.set(result, rs);
            }
        } catch (Exception e) {
            log.error("Instance creation failure", e);
        }
        return result;
    }
}
