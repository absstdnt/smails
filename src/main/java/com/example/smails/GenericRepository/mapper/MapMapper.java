package com.example.smails.GenericRepository.mapper;

import com.example.smails.GenericRepository.GenericRepository;
import com.example.smails.GenericRepository.mapper.setter.CastSetter;
import com.example.smails.GenericRepository.mapper.setter.EntitySetter;
import com.example.smails.GenericRepository.mapper.setter.Setter;
import com.example.smails.GenericRepository.mapper.setter.SimpleSetter;
import com.example.smails.controller.BeanHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.Map;

import static com.example.smails.GenericRepository.mapper.Utils.getColumnLabel;
import static com.example.smails.GenericRepository.mapper.Utils.setterCanBeGenerated;

/**
 * @author user
 */
public class MapMapper implements Mapper {

    private static final Logger log = LogManager.getLogger(MapMapper.class);

    protected Map<String, Setter> setters = new HashMap<>();

    public void setSetter(String field, Setter setter) {
        setters.put(field, setter);
    }


    @Override
    public Map<String, Object> map(ResultSet rs) {
        Map<String, Object> result = new HashMap<>();
        try {
            ResultSetMetaData md = rs.getMetaData();
            for (int i = 1; i <= md.getColumnCount(); i++) {
                String columnLabel = md.getColumnLabel(i);
                result.put(columnLabel, rs.getObject(columnLabel));
            }
        } catch (Exception e) {
            log.error("Instance creation failure", e);
        }
        return result;
    }
}
