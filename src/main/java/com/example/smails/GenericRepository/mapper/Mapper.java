package com.example.smails.GenericRepository.mapper;

import com.example.smails.GenericRepository.mapper.setter.Setter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author user
 */
public interface Mapper<T> {

    T map(ResultSet rs);

    default void setSetter(String column, Setter<T> setter) {};

    default void removeGetter(String column) {};

    default void resetCaches() {}

    default int unmap(PreparedStatement ps, T object, boolean update) {return 0;}

    default String getColumnNames() {return "";}

    default String getColumnNamesForUpdate() {return "";}

    default String getColumnMarks() {return "";}

    default Object getId(T object) {return 0;}

    default void setId(T object, ResultSet rs) {}

}
