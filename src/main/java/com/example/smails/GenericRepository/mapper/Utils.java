package com.example.smails.GenericRepository.mapper;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author user
 */
public final class Utils {
    private Utils() {
    }

    public static Class getSqlType(Class<?> type) {
        if (type == LocalDateTime.class) return Timestamp.class;
        if (type == LocalDate.class) return Date.class;
        return type;
    }

    private static String uppercaseFirstLetter(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    public static String getSetterName(String fieldName) {
        return "set" + uppercaseFirstLetter(fieldName);
    }

    public static String getGetterName(String fieldName) {
        return "get" + uppercaseFirstLetter(fieldName);
    }

    public static String getColumnLabel(Field field) {
        String fieldName = field.getName();
        //TODO annotations
        if ("deliveryDate".equals(fieldName)) return "delivery_date";
        if ("account".equals(fieldName)) return "acc";
        return fieldName;
    }


    public static boolean setterCanBeGenerated(Class<?> type) {
        return type.isPrimitive()
                || type == String.class
                || type == BigDecimal.class
                || type == LocalDate.class
                || type == LocalDateTime.class;
    }

}
