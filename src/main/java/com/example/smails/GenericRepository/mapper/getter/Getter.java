package com.example.smails.GenericRepository.mapper.getter;

import com.example.smails.controller.exceptions.AllowedException;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

/**
 * @author user
 */
@FunctionalInterface
public interface Getter<T> {
    Object get(T obj) throws AllowedException, InvocationTargetException, IllegalAccessException, SQLException;
}
