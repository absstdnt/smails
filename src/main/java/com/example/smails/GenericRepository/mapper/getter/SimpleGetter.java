package com.example.smails.GenericRepository.mapper.getter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

/**
 * @author user
 */
public class SimpleGetter<T> implements Getter<T> {
    private static final Logger log = LogManager.getLogger(SimpleGetter.class);

    private final Method m;
    private final String columnLabel;

    public SimpleGetter(Method m, String columnLabel) {
        this.m = m;
        this.columnLabel = columnLabel;
    }

    public Object get(T obj) throws InvocationTargetException, IllegalAccessException, SQLException {
        Object value = null;
        try {
            return m.invoke(obj);
        } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
            log.error("get error: " + columnLabel + " " + obj, e);
            throw e;
        }
    }
}
