package com.example.smails.GenericRepository.mapper.setter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author user
 */
public class CastSetter<T> implements Setter<T> {
    private static final Logger log = LogManager.getLogger(CastSetter.class);

    private final Method setMethod;
    private final Method castMethod;
    private final String columnLabel;

    public CastSetter(Method setMethod, String columnLabel, Class sqlType, Class type) throws NoSuchMethodException {
        this.setMethod = setMethod;
        try {
            this.castMethod = getClass().getMethod("cast", sqlType);
        } catch (NoSuchMethodException e) {
            log.error("No sutable cast method found: " + sqlType + " -> " + type);
            throw e;
        }
        this.columnLabel = columnLabel;
    }

    public static LocalDateTime cast(Timestamp timestamp) {
        if (timestamp == null) return LocalDateTime.of(1, 1, 1, 0, 0, 0);
        return timestamp.toLocalDateTime();
    }

    public static LocalDate cast(Date date) {
        if (date == null) return LocalDate.of(1, 1, 1);
        return date.toLocalDate();
    }

    public void set(T obj, ResultSet rs) throws InvocationTargetException, IllegalAccessException, SQLException {
        Object value = null;
        try {
            value = rs.getObject(columnLabel);
            setMethod.invoke(obj, castMethod.invoke(null, value));
        } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException | SQLException e) {
            log.error("set error: " + columnLabel + " " + obj + " " + value, e);
            throw e;
        }
    }
}
