package com.example.smails.GenericRepository.mapper.setter;

import com.example.smails.GenericRepository.GenericRepository;
import com.example.smails.controller.exceptions.AllowedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;

/**
 * @author user
 */
public class EntitySetter<T> implements Setter<T> {
    private static final Logger log = LogManager.getLogger(EntitySetter.class);

    private final Method m;
    private final String columnLabel;
    private final GenericRepository repo;
    private Map<Object, Object> cache;

    public EntitySetter(Method m, String columnLabel, GenericRepository repo) {
        this.m = m;
        this.columnLabel = columnLabel + "_id";
        this.repo = repo;
    }

    public EntitySetter(Method m, String columnLabel, GenericRepository repo, Map<Object, Object> cache) {
        this(m, columnLabel, repo);
        this.cache = cache;
    }

    public void set(T obj, ResultSet rs) throws InvocationTargetException, IllegalAccessException, SQLException {

        Object id;
        try {
            id = rs.getObject(columnLabel);
        } catch (SQLException e) {
            log.error("EntitySetter set error: Column not found in result set: " + columnLabel + " " + obj, e);
            throw e;
        }

        Object value = null;
        if (cache != null) value = cache.get(id);
        if (value == null) {
            Optional opt = repo.findById(id);
            if (!opt.isPresent()) throw new AllowedException("No " + repo.getClass() + " with ID=" + id);
            value = opt.get();
            if (cache != null) cache.put(id, value);
        }

        try {
            m.invoke(obj, value);
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error("set error: " + columnLabel + " " + obj, e);
            throw e;
        }
    }
}
