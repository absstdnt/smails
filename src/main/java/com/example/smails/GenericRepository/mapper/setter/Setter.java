package com.example.smails.GenericRepository.mapper.setter;

import com.example.smails.controller.exceptions.AllowedException;

import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author user
 */
@FunctionalInterface
public interface Setter<T> {
    void set(T obj, ResultSet rs) throws AllowedException, InvocationTargetException, IllegalAccessException, SQLException;
}
