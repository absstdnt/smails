package com.example.smails.GenericRepository.mapper.setter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author user
 */
public class SimpleCastSetter<T> implements Setter<T> {
    private static final Logger log = LogManager.getLogger(SimpleCastSetter.class);

    private final Method setMethod;
    private final String columnLabel;
    private final Class javaType;

    public SimpleCastSetter(Method setMethod, String columnLabel, Class sqlType, Class javaType) throws NoSuchMethodException {
        this.setMethod = setMethod;
        this.columnLabel = columnLabel;
        this.javaType = javaType;
    }

    public static LocalDateTime cast(Timestamp timestamp) {
        return timestamp.toLocalDateTime();
    }
    public static LocalDate cast(Date date) {
        return date.toLocalDate();
    }

    public void set(T obj, ResultSet rs) throws InvocationTargetException, IllegalAccessException, SQLException {
        Object value = null;
        try {
            value = rs.getObject(columnLabel, javaType);
            setMethod.invoke(obj, value);
        } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException | SQLException e) {
            log.error("set error: " + columnLabel + " " + obj + " " + value, e);
            throw e;
        }
    }
}
