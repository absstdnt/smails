package com.example.smails.GenericRepository.mapper.setter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author user
 */
public class SimpleSetter<T> implements Setter<T> {
    private static final Logger log = LogManager.getLogger(SimpleSetter.class);

    private final Method m;
    private final String columnLabel;

    public SimpleSetter(Method m, String columnLabel) {
        this.m = m;
        this.columnLabel = columnLabel;
    }

    public void set(T obj, ResultSet rs) throws InvocationTargetException, IllegalAccessException, SQLException {
        Object value = null;
        try {
            value = rs.getObject(columnLabel);
            m.invoke(obj, value);
        } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException | SQLException e) {
            log.error("set error: " + columnLabel + " " + obj + " " + value, e);
            throw e;
        }
    }
}
