package com.example.smails.account;


import java.util.Arrays;

public class Account {
    private int id;
    private String email;
    private String password;
    private Role role;

    public Account() {
        this.email = "Guest";
        this.role = Role.ROLE_USER;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

//    public void setRole(String role) {
//        this.role = Role.convertToEntityAttribute(role);
//    }

    @Override
    public String toString() {
        return email + " [" + role.getDisplayValue() + ']';
    }

    public boolean isAuthenticated() {
        return id > 0;
    }

    public boolean hasRole(Role role) {
        return this.role == role;
    }

    public boolean hasRole(String role) {
        return hasRole(Role.valueOf("ROLE_"+role));
    }

    public boolean hasAnyRole(String... roles) {
        return Arrays.stream(roles).anyMatch(this::hasRole);
    }

}
