package com.example.smails.account;

import com.example.smails.GenericRepository.GenericRepositoryImpl;

/**
 * @author user
 */
public class AccountRepository extends GenericRepositoryImpl<Account, Integer> {
    {
        init(Account.class);
        defaultMapper.setSetter("role", (o, rs) -> o.setRole(Role.convertToEntityAttribute(rs.getString("role"))));
    }
}
