package com.example.smails.account;

import com.example.smails.controller.BeanHolder;

import java.util.Optional;

public class AccountService {

    AccountRepository accountRepository = BeanHolder.getBean(AccountRepository.class);

    public Optional<Account> findByEmail(String email) {
        return accountRepository.findBy("email", email);
    }
}
