package com.example.smails.account;

import java.util.stream.Stream;

/**
 * @author user
 */
public enum Role {

    ROLE_USER("U", "User"), ROLE_AUSER("A", "Authenticated user"), ROLE_MANAGER("M", "Manager");

    final private String code;
    final private String displayValue;

    Role(String code, String displayValue) {
        this.code = code;
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public String getCode() {
        return code;
    }

    public static String convertToDatabaseColumn(Role role) {
        return role.getCode();
    }

    public static Role convertToEntityAttribute(String code) {
        return Stream.of(Role.values())
                .filter(c -> c.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}