package com.example.smails.controller;

import com.example.smails.account.AccountRepository;
import com.example.smails.destination.DestinationRepository;
import com.example.smails.destination.DestinationService;
import com.example.smails.distance.DistanceRepository;
import com.example.smails.distance.DistanceService;
import com.example.smails.account.AccountService;
import com.example.smails.invoice.InvoiceRepository;
import com.example.smails.invoice.InvoiceService;
import com.example.smails.order.OrderRepository;
import com.example.smails.order.OrderService;
import com.example.smails.order.PriceService;
import com.example.smails.orderType.OrderTypeRepository;
import com.example.smails.orderType.OrderTypeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author user
 */
public class BeanHolder {

    private static final Logger log = LogManager.getLogger(BeanHolder.class);

    private static Map<Class, Object> beanHolder = new HashMap<>();
    private static Set<String> classNames = new HashSet<>();

    static {
        classNames.add("com.example.smails.account.AccountRepository");
        classNames.add("com.example.smails.destination.OrderTypeRepository");
        classNames.add("com.example.smails.distance.DistanceRepository");
        classNames.add("com.example.smails.order.OrderRepository");
        classNames.add("com.example.smails.order.OrderTypeRepository");
        classNames.add("com.example.smails.invoice.InvoiceRepository");
        beanHolder.put(PriceService.class, new PriceService());
        beanHolder.put(AccountRepository.class, new AccountRepository());
        beanHolder.put(AccountService.class, new AccountService());
        beanHolder.put(DestinationRepository.class, new DestinationRepository());
        beanHolder.put(DestinationService.class, new DestinationService());
        beanHolder.put(DistanceRepository.class, new DistanceRepository());
        beanHolder.put(DistanceService.class, new DistanceService());
        beanHolder.put(OrderRepository.class, new OrderRepository());
        beanHolder.put(OrderService.class, new OrderService());
        beanHolder.put(OrderTypeRepository.class, new OrderTypeRepository());
        beanHolder.put(OrderTypeService.class, new OrderTypeService());
        beanHolder.put(InvoiceRepository.class, new InvoiceRepository());
        beanHolder.put(InvoiceService.class, new InvoiceService());
    }

    private BeanHolder() {
    }

    public static <T> T getBean(Class<T> key) {
        return (T) beanHolder.get(key);
    }

    public static boolean contains(String className) {
        return classNames.contains(className);
    }
}