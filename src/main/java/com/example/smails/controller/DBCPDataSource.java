package com.example.smails.controller;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.Properties;

/**
 * @author user
 */
public class DBCPDataSource {

    private static final Logger log = LogManager.getLogger(DBCPDataSource.class);

    private static final BasicDataSource ds = new BasicDataSource();

    static {
        //todo refactor
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        Properties prop = new Properties();
        try (InputStream inp = classLoader.getResourceAsStream("app.properties")) {
            prop.load(inp);
        } catch (IOException e) {
            log.error("Error accessing properties", e);
            throw new RuntimeException(e);
        }
        ds.setUrl(prop.getProperty("datasource.url"));
        ds.setUsername(prop.getProperty("datasource.username"));
        ds.setPassword(prop.getProperty("datasource.password"));

        ds.setMinIdle(3);
        ds.setMaxIdle(5);
        ds.setMaxOpenPreparedStatements(20);
    }

    private DBCPDataSource() {
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}