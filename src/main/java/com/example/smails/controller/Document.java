package com.example.smails.controller;

import java.time.LocalDateTime;

/**
 * @author user
 */
public interface Document {
    int getNumber();
    LocalDateTime getCreated();
}
