package com.example.smails.controller;

import com.example.smails.controller.command.*;
import com.example.smails.account.Account;
import com.example.smails.destination.DestinationService;
import com.example.smails.invoice.InvoiceService;
import com.example.smails.order.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Servlet extends HttpServlet {

    private static final Logger log = LogManager.getLogger(Servlet.class);

    private final Map<String, Command> commands = new HashMap<>();

    public void init(ServletConfig servletConfig) {

        commands.put("GET/logout",
                new LogOutCommand());
        commands.put("GET/login",
                (r) -> "/login.jsp");
        commands.put("POST/login",
                new LoginCommandPost());
        commands.put("GET/about",
                (r) -> "/about.jsp");
        commands.put("GET",
                (r) -> ((Account) r.getSession().getAttribute("account")).isAuthenticated()
                        ? "/WEB-INF/homeSignedIn.jsp"
                        : "/homeNotSignedIn.jsp");
        commands.put("GET/distance",
                new DistanceListCommand());

        commands.put("GET/destination",
                new DestinationListCommand());
        commands.put("GET/destination/add",
                (r) -> "/WEB-INF/destination-form.jsp");
        commands.put("GET/destination/edit",
                new DestinationEditCommand());
        commands.put("POST/destination/save",
                new DestinationSaveCommand());
        commands.put("GET/destination/delete",
                //new DestinationDeleteCommand());
                new GenericDeleteCommand(DestinationService.class));

        commands.put("GET/order",
                new OrderListCommand());
//        commands.put("GET/order/add",
//                (r) -> "/WEB-INF/order-form.jsp");
        commands.put("GET/order/edit",
                new OrderEditCommand());
        commands.put("POST/order/save",
                new OrderSaveCommand());
        commands.put("GET/order/delete",
                new GenericDeleteCommand(OrderService.class));

        commands.put("GET/invoice",
                new InvoiceListCommand());
        commands.put("GET/invoice/edit",
                new InvoiceEditCommand());
        commands.put("POST/invoice/save",
                new InvoiceSaveCommand());
        commands.put("GET/invoice/pay",
                new InvoicePayCommand());
        commands.put("GET/invoice/delete",
                new GenericDeleteCommand(InvoiceService.class));

        commands.put("GET/calculate",
                new OrderEditCommand());
        commands.put("POST/calculate",
                new CalculateCommand());

        commands.put("exception",
                new ExceptionCommand());
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response, "GET");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        processRequest(request, response, "POST");
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse res, String method)
            throws ServletException, IOException {

        String path = req.getContextPath() + req.getServletPath();
        String commandName = method + req.getRequestURI().substring(path.length());
        if (commandName.endsWith("/")) commandName = commandName.substring(0, commandName.length() - 1);
        Command command = commands.get(commandName);
        String page;
        if (command != null) {
            page = command.execute(req);
        } else {
            String description = "Command not supported " + commandName;
            log.warn("@@@@ 404 " + description);
            req.setAttribute("description", description);
            page = "/warning.jsp";
        }

        if (page.startsWith("redirect:")) {
            res.sendRedirect(page.substring(9));
        } else if (page.startsWith("response:")) {
            String responseData = page.substring(9);
            if (responseData.startsWith("{")) {
                res.setContentType("application/json");
                //TODO async validation messages
            }
            res.getWriter().write(responseData);
        } else {
            req.getRequestDispatcher(page).forward(req, res);
        }
    }

}
