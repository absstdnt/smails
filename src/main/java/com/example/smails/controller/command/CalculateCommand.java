package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.order.OrderForm;
import com.example.smails.order.OrderService;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static com.example.smails.controller.command.CommandUtility.*;


public class CalculateCommand implements Command {
    private static final Logger log = LogManager.getLogger(CalculateCommand.class);

    private final OrderService orderService = BeanHolder.getBean(OrderService.class);

    @Override
    public String execute(HttpServletRequest request) {
        OrderForm orderForm = new OrderForm();

        Map<String, String> errors = new HashMap<>();

        orderForm.setFrom_id(getIntPositiveParam(request, "from_id", errors));
        orderForm.setTo_id(getIntPositiveParam(request, "to_id", errors));
//        orderForm.setType_id(getIntPositiveParam(request, "type_id", errors));
        orderForm.setWeight(getIntPositiveParam(request, "weight", errors));
        orderForm.setLength(getIntPositiveParam(request, "length", errors));
        orderForm.setWidth(getIntPositiveParam(request, "width", errors));
        orderForm.setHeight(getIntPositiveParam(request, "height", errors));
//        orderForm.setDeliveryDate(getFutureDateParam(request, "deliveryDate", errors));
//        orderForm.setAddress(request.getParameter("address"));

        if (!errors.isEmpty()) {
            return "response:" + new Gson().toJson(errors);
        }

        return "response:" + orderService.getTotal(orderForm);
    }


}
