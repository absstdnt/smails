package com.example.smails.controller.command;

import com.example.smails.account.Account;
import com.example.smails.controller.exceptions.AllowedException;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

public class CommandUtility {

    private CommandUtility() {
    }

    public static int getId(HttpServletRequest request) {
        String param = request.getParameter("id");
        if (param == null) return 0;
        //throw new AllowedException("ID not specified");

        if (param.isEmpty()) return 0;

        int id;

        try {
            id = Integer.parseInt(param);
        } catch (NumberFormatException ex) {
            throw new AllowedException("Wrong ID format:" + param, ex);
        }
        return id;
    }

    public static String getParam(HttpServletRequest request, String name) {
        String param = request.getParameter(name);
        if (param == null)
            throw new AllowedException("Parameter " + name + " not specified");
        return param;
    }

    public static int getIntParam(HttpServletRequest request, String name) {
        String param = getParam(request, name);

        if (param.isEmpty()) return 0;

        int id;

        try {
            id = Integer.parseInt(param);
        } catch (NumberFormatException ex) {
            throw new AllowedException("Wrong param " + name + " format:" + param, ex);
        }
        return id;
    }


    public static int getIntPositiveParam(HttpServletRequest request, String paramName, Map<String, String> errors) {

        String param = getParam(request, paramName);
        int value = 0;

        if (param.isEmpty()) {
            errors.put(paramName, "Field \"" + paramName + "\" cannot be empty.");
            return value;
        }

        try {
            value = Integer.parseInt(param);
        } catch (NumberFormatException ex) {
            //throw new AllowedException("Wrong param " + paramName + " format:" + param, ex);
            errors.put(paramName, "Field \"" + paramName + "\" should be a number.");
            return value;
        }

        if (value <= 0) errors.put(paramName, "Field \"" + paramName + "\" should be positive.");

        return value;
    }

    public static BigDecimal getBigDecimalPositiveParam(HttpServletRequest request, String paramName, Map<String, String> errors) {

        String param = getParam(request, paramName);
        BigDecimal value = null;

        if (param.isEmpty()) {
            errors.put(paramName, "Field \"" + paramName + "\" cannot be empty.");
            return value;
        }

        try {
            value = new BigDecimal(param);
        } catch (NumberFormatException ex) {
            //throw new AllowedException("Wrong param " + paramName + " format:" + param, ex);
            errors.put(paramName, "Field \"" + paramName + "\" should be a decimal number.");
            return value;
        }

        if (value.compareTo(BigDecimal.ZERO) <= 0) errors.put(paramName, "Field \"" + paramName + "\" should be positive.");

        return value;
    }

    public static String getStringNotEmptyParam(HttpServletRequest request, String paramName, Map<String, String> errors) {

        String param = getParam(request, paramName);

        if (param.isEmpty())
            errors.put(paramName, "Field \"" + paramName + "\" cannot be empty.");

        return param;
    }

    public static LocalDate getFutureDateParam(HttpServletRequest request, String paramName, Map<String, String> errors) {

        String param = getParam(request, paramName);
        LocalDate value = null; //= LocalDate.of(1, 1, 1);

        if (param.isEmpty()) {
            errors.put(paramName, "Field \"" + paramName + "\" cannot be empty.");
            return value;
        }

        try {
            value = LocalDate.parse(param);
        } catch (Exception ex) {
            //throw new AllowedException("Wrong param " + paramName + " format:" + param, ex);
            errors.put(paramName, "Field \"" + paramName + "\" should be a date.");
            return value;
        }

        if (value.isBefore(LocalDate.now())) errors.put(paramName, "Field \"" + paramName + "\" should be in future.");

        return value;
    }

    public static Account getAccount(HttpServletRequest request) {
        return (Account) request.getSession().getAttribute("account");
    }

}
