package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.destination.DestinationService;

import javax.servlet.http.HttpServletRequest;

import static com.example.smails.controller.command.CommandUtility.getId;

public class DestinationEditCommand implements Command {

    private final DestinationService destinationService = BeanHolder.getBean(DestinationService.class);

    @Override
    public String execute(HttpServletRequest request) {
        int id = getId(request);
        request.setAttribute("destination", destinationService.findById(id));
        return "/WEB-INF/destination-form.jsp";
    }
}
