package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.destination.DestinationService;

import javax.servlet.http.HttpServletRequest;

public class DestinationListCommand implements Command {

    private final DestinationService destinationService = BeanHolder.getBean(DestinationService.class);

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("list", destinationService.findAll());
        return "/WEB-INF/destination-list.jsp";
    }
}
