package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.destination.Destination;
import com.example.smails.destination.DestinationService;

import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.smails.controller.command.CommandUtility.getId;
import static com.example.smails.controller.command.CommandUtility.getStringNotEmptyParam;

public class DestinationSaveCommand implements Command {

    private final DestinationService DestinationService = BeanHolder.getBean(DestinationService.class);

    @Override
    public String execute(HttpServletRequest request) {
        Destination destination = new Destination();

        Map<String, String> errors = new HashMap<>();

        destination.setId(getId(request));
        destination.setName(getStringNotEmptyParam(request, "name", errors));
        if (!errors.isEmpty()) {
            request.setAttribute("destination", destination);
            request.setAttribute("errors", errors);
            return "/WEB-INF/destination-form.jsp";
        }

        DestinationService.save(destination);
        return "redirect:.";
    }
}
