package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.distance.DistanceService;

import javax.servlet.http.HttpServletRequest;

public class DistanceListCommand implements Command {

    private final DistanceService distanceService = BeanHolder.getBean(DistanceService.class);


    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("list", distanceService.findAllRepresentations());
        return "/WEB-INF/distance-list.jsp";
    }
}
