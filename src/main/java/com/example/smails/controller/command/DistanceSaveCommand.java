package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.distance.Distance;
import com.example.smails.distance.DistanceService;

import javax.servlet.http.HttpServletRequest;

public class DistanceSaveCommand implements Command {

    private final DistanceService distanceService = BeanHolder.getBean(DistanceService.class);


    @Override
    public String execute(HttpServletRequest request) {
        Distance distance = new Distance();
        distanceService.save(distance);
        return "redirect:/distance";
    }
}
