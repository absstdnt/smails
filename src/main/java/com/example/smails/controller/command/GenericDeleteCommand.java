package com.example.smails.controller.command;

import com.example.smails.controller.EntityService;
import com.example.smails.controller.BeanHolder;

import javax.servlet.http.HttpServletRequest;

import static com.example.smails.controller.command.CommandUtility.getId;

public class GenericDeleteCommand implements Command {

    private final EntityService entityService;

    public GenericDeleteCommand(Class clazz) {
        this.entityService = (EntityService) BeanHolder.getBean(clazz);
    }

    @Override
    public String execute(HttpServletRequest request) {
        int id = getId(request);
        entityService.deleteById(id);
        return "redirect:.";
    }
}
