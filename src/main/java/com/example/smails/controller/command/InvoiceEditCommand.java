package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.invoice.InvoiceForm;
import com.example.smails.invoice.InvoiceService;
import com.example.smails.order.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.example.smails.controller.command.CommandUtility.getId;
import static com.example.smails.controller.command.CommandUtility.getIntParam;

public class InvoiceEditCommand implements Command {
    private static final Logger log = LogManager.getLogger(InvoiceEditCommand.class);

    private final InvoiceService invoiceService = BeanHolder.getBean(InvoiceService.class);
    private final OrderService orderService = BeanHolder.getBean(OrderService.class);

    @Override
    public String execute(HttpServletRequest request) {
        int id = getId(request);
        if (id > 0) // edit
            request.setAttribute("invoice", invoiceService.findById(id, CommandUtility.getAccount(request)));
        else {
            int order_id = getIntParam(request, "order_id");
            request.setAttribute("invoice", new InvoiceForm(orderService.findById(order_id, CommandUtility.getAccount(request))));
        }
        return "/WEB-INF/invoice-form.jsp";
    }


}
