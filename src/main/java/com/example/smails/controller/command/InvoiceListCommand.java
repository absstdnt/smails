package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.invoice.InvoiceService;

import javax.servlet.http.HttpServletRequest;

public class InvoiceListCommand implements Command {

    private final InvoiceService invoiceService = BeanHolder.getBean(InvoiceService.class);

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("list", invoiceService.findAll(CommandUtility.getAccount(request)));
        return "/WEB-INF/invoice-list.jsp";
    }


}
