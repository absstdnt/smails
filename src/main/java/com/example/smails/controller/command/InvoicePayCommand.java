package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.invoice.InvoiceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.example.smails.controller.command.CommandUtility.getId;

public class InvoicePayCommand implements Command {
    private static final Logger log = LogManager.getLogger(InvoicePayCommand.class);

    private final InvoiceService invoiceService = BeanHolder.getBean(InvoiceService.class);

    @Override
    public String execute(HttpServletRequest request) {
        invoiceService.payById(getId(request), CommandUtility.getAccount(request));
        return "redirect:.";
    }


}
