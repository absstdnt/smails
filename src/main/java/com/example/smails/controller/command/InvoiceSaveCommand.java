package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.invoice.InvoiceForm;
import com.example.smails.invoice.InvoiceService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static com.example.smails.controller.command.CommandUtility.*;

public class InvoiceSaveCommand implements Command {

    private final InvoiceService invoiceService = BeanHolder.getBean(InvoiceService.class);

    @Override
    public String execute(HttpServletRequest request) {

        InvoiceForm invoiceForm = new InvoiceForm();

        Map<String, String> errors = new HashMap<>();

        invoiceForm.setId(getIntParam(request, "id"));
        //TODO set acc_id in SQL
        invoiceForm.setAcc_id(getIntPositiveParam(request, "acc_id", errors));
        invoiceForm.setOrder_id(getIntPositiveParam(request, "order_id", errors));
        invoiceForm.setTotal(getBigDecimalPositiveParam(request, "total", errors));
        invoiceForm.setComment(request.getParameter("comment"));

        if (!errors.isEmpty()) {
            request.setAttribute("invoice", invoiceForm);
            request.setAttribute("errors", errors);
            return "/WEB-INF/invoice-form.jsp";
        }

        invoiceService.save(invoiceForm);
        return "redirect:.";
    }
}
