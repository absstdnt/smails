package com.example.smails.controller.command;

import com.example.smails.account.Account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogOutCommand implements Command {
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("account", new Account());
        return "redirect:.";
    }
}
