package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.account.Account;
import com.example.smails.account.AccountService;
import com.example.smails.controller.Servlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

public class LoginCommandPost implements Command {

    private static final Logger log = LogManager.getLogger(LoginCommandPost.class);

    private final AccountService accountService = BeanHolder.getBean(AccountService.class);


    @Override
    public String execute(HttpServletRequest request) {
        String name = request.getParameter("name");
        String pass = request.getParameter("pass");
        //todo: check pass with DB

        Optional<Account> account = accountService.findByEmail(name);
        if (!account.isPresent()) {
            request.setAttribute("message", "User " + name + " not found");
            request.setAttribute("name", name);
            return "/login.jsp";
        }

        if (!BCrypt.checkpw(pass, account.get().getPassword())) {
            log.warn("Wrong password by {}", name);
            request.setAttribute("message", "Wrong password");
            request.setAttribute("name", name);
            return "/login.jsp";
        }

        HttpSession session = request.getSession();
        session.setAttribute("account", account.get());
        return "redirect:.";
    }
}
