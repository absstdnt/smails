package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.destination.DestinationService;
import com.example.smails.order.OrderService;
import com.example.smails.orderType.OrderTypeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.example.smails.controller.command.CommandUtility.getId;

public class OrderEditCommand implements Command {
    private static final Logger log = LogManager.getLogger(OrderEditCommand.class);

    private final OrderService orderService = BeanHolder.getBean(OrderService.class);
    private final DestinationService destinationService = BeanHolder.getBean(DestinationService.class);
    private final OrderTypeService orderTypeService = BeanHolder.getBean(OrderTypeService.class);

    @Override
    public String execute(HttpServletRequest request) {
        int id = getId(request);
        if (id > 0) request.setAttribute("order", orderService.findById(id, CommandUtility.getAccount(request)));
        request.setAttribute("destinations", destinationService.findAll());
        request.setAttribute("orderTypes", orderTypeService.findAll());
        return "/WEB-INF/order-form.jsp";
    }


}
