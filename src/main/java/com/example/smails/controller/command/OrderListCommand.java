package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.order.OrderService;

import javax.servlet.http.HttpServletRequest;

public class OrderListCommand implements Command {

    private final OrderService orderService = BeanHolder.getBean(OrderService.class);

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("list", orderService.findAllRepresentations(CommandUtility.getAccount(request)));
        return "/WEB-INF/order-list.jsp";
    }


}
