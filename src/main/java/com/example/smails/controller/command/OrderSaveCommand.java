package com.example.smails.controller.command;

import com.example.smails.controller.BeanHolder;
import com.example.smails.destination.DestinationService;
import com.example.smails.order.OrderForm;
import com.example.smails.order.OrderService;
import com.example.smails.orderType.OrderTypeService;

import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

import static com.example.smails.controller.command.CommandUtility.*;

public class OrderSaveCommand implements Command {

    private final OrderService orderService = BeanHolder.getBean(OrderService.class);
    private final DestinationService destinationService = BeanHolder.getBean(DestinationService.class);
    private final OrderTypeService orderTypeService = BeanHolder.getBean(OrderTypeService.class);

    @Override
    public String execute(HttpServletRequest request) {
        OrderForm orderForm = new OrderForm();

        Map<String, String> errors = new HashMap<>();

        orderForm.setId(getIntParam(request, "id"));
        orderForm.setFrom_id(getIntPositiveParam(request, "from_id", errors));
        orderForm.setTo_id(getIntPositiveParam(request, "to_id", errors));
        orderForm.setType_id(getIntPositiveParam(request, "type_id", errors));
        orderForm.setWeight(getIntPositiveParam(request, "weight", errors));
        orderForm.setLength(getIntPositiveParam(request, "length", errors));
        orderForm.setWidth(getIntPositiveParam(request, "width", errors));
        orderForm.setHeight(getIntPositiveParam(request, "height", errors));
        orderForm.setDeliveryDate(getFutureDateParam(request, "deliveryDate", errors));
        orderForm.setAddress(request.getParameter("address"));

        if (!errors.isEmpty()) {
            request.setAttribute("order", orderForm);
            request.setAttribute("errors", errors);
            request.setAttribute("destinations", destinationService.findAll());
            request.setAttribute("orderTypes", orderTypeService.findAll());
            return "/WEB-INF/order-form.jsp";
        }

        orderService.save(orderForm, CommandUtility.getAccount(request));
        return "redirect:.";
    }
}
