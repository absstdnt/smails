package com.example.smails.controller.exceptions;

/**
 * @author user
 */
public class AllowedException extends RuntimeException {

    public AllowedException() {
        super();
    }

    public AllowedException(String message) {
        super(message);
    }

    public AllowedException(String message, Throwable cause) {
        super(message, cause);
    }
}