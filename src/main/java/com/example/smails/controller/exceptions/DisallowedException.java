package com.example.smails.controller.exceptions;

import com.example.smails.account.Account;

/**
 * @author user
 */
public class DisallowedException extends AllowedException {

    Account account;

    public DisallowedException() {
        super();
    }

    public DisallowedException(String message) {
        super(message);
    }
    public DisallowedException(String message, Account account) {
        super(message);
        this.account = account;
    }

    public DisallowedException(String message, Throwable cause) {
        super(message, cause);
    }
}