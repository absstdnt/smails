package com.example.smails.controller.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class AppFilter implements Filter {

    private static final Logger log = LogManager.getLogger(AppFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String servletPath = req.getServletPath();

        if (servletPath.startsWith("/static/")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

//        if ("/login".equals(servletPath)
//                || "/logout".equals(servletPath)
//                || "/about".equals(servletPath)
//                || "/".equals(servletPath)
//                || "/home".equals(servletPath)) {
            req.getRequestDispatcher("/app"+servletPath).forward(servletRequest, servletResponse);
//            return;
//        } //todo 404


    }

    @Override
    public void destroy() {
    }
}
