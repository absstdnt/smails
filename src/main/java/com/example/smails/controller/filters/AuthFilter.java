package com.example.smails.controller.filters;

import com.example.smails.account.Account;
import com.example.smails.account.Role;
import com.example.smails.controller.command.CommandUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthFilter implements Filter {

    private static final Logger log = LogManager.getLogger(AuthFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        Account account = CommandUtility.getAccount(req);

        String path = req.getServletPath();
        if (path.endsWith("/")) path = path.substring(0, path.length() - 1);

        // no restrictions
        if (path.startsWith("/static")
                || path.equals("")
                || path.equals("/about")
                || path.equals("/login")
                || path.equals("/distance")
//                || path.equals("/order/edit")
                || path.equals("/calculate")) {
            filterChain.doFilter(request, response);
            return;
        }

        // auser
        if (account.hasRole(Role.ROLE_AUSER))
            if (path.startsWith("/order")
                    || path.equals("/invoice")
                    || path.equals("/invoice/pay")
                    || path.equals("/logout")) {
                filterChain.doFilter(request, response);
                return;
            }

        // manager
        if (account.hasRole(Role.ROLE_MANAGER))
            if (path.startsWith("/order")
                    || path.startsWith("/distance")
                    || path.startsWith("/destination")
                    || path.startsWith("/report")
                    || path.equals("/invoice")
                    || path.equals("/invoice/add")
                    || path.equals("/invoice/edit")
                    || path.equals("/invoice/save")
                    || path.equals("/invoice/delete")
                    || path.equals("/logout")) {
                filterChain.doFilter(request, response);
                return;
            }

        //throw new DisallowedException("Request to "+path+" "+account, account);
        log.warn("Request to {} from {} ", path, account);
        //res.sendRedirect(req.getContextPath() + "/login");
        req.setAttribute("message", account.getEmail() + " - Access not granted.");
        res.setStatus(403);
        req.getRequestDispatcher("/login.jsp").forward(req, res);
    }

    @Override
    public void destroy() {

    }
}
