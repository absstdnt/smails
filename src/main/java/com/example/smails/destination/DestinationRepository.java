package com.example.smails.destination;

import com.example.smails.GenericRepository.GenericRepositoryImpl;

/**
 * @author user
 */
public class DestinationRepository extends GenericRepositoryImpl<Destination, Integer> {
    {
        init(Destination.class);
    }

}
