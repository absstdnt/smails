package com.example.smails.destination;


import com.example.smails.controller.EntityService;
import com.example.smails.controller.exceptions.AllowedException;
import com.example.smails.controller.BeanHolder;

import java.util.List;

/**
 * @author user
 */
public class DestinationService implements EntityService {

    private final DestinationRepository destinationRepository = BeanHolder.getBean(DestinationRepository.class);

    public List<Destination> findAll() {
        return destinationRepository.findAll();
    }

    public Destination findById(int id) {
        return destinationRepository.findById(id).orElseThrow(() -> new AllowedException("No OrderType with id=" + id));
    }

    public void save(Destination destination) {
        destinationRepository.save(destination);
    }

    public void deleteById(int id) {
        destinationRepository.deleteById(id);
    }
}
