package com.example.smails.distance;

import com.example.smails.destination.Destination;

import java.io.Serializable;

/**
 * @author user
 */

//@IdClass(DistanceId.class)
public class Distance implements Serializable {

    Destination from;

    Destination to;

    int value;

    public Destination getFrom() {
        return from;
    }

    public void setFrom(Destination from) {
        this.from = from;
    }

    public Destination getTo() {
        return to;
    }

    public void setTo(Destination to) {
        this.to = to;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}