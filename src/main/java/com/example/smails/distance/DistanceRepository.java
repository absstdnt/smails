package com.example.smails.distance;

import com.example.smails.GenericRepository.GenericRepositoryImpl;
import com.example.smails.controller.exceptions.AllowedException;
import com.example.smails.controller.BeanHolder;
import com.example.smails.destination.DestinationRepository;

import java.util.List;
import java.util.Map;

/**
 * @author user
 */
public class DistanceRepository extends GenericRepositoryImpl<Distance, Integer> {

    private DestinationRepository destinationRepository = BeanHolder.getBean(DestinationRepository.class);

    {
        init(Distance.class);
//        setSetter("from_id", (o, rs) -> {
//            int id = rs.getInt("from_id");
//            o.setFrom(destinationRepository.findById(id)
//                    .orElseThrow(() -> new AllowedException("No destination with ID=" + id)));
//        });
//        setSetter("to_id", (o, rs) -> {
//            int id = rs.getInt("to_id");
//            o.setTo(destinationRepository.findById(id)
//                    .orElseThrow(() -> new AllowedException("No destination with ID=" + id)));
//        });
    }

    public List<Map<String, Object>> findAllRepresentations() {
        String sql = "SELECT df.name AS from_name, dt.name AS to_name, d.value AS value FROM "
                + table + " d\n"
                + "LEFT JOIN destination df ON d.from_id = df.id\n"
                + "LEFT JOIN destination dt ON d.to_id = dt.id\n";
        return super.findAll(sql);
    }


}
