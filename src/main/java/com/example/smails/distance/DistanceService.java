package com.example.smails.distance;

import com.example.smails.controller.BeanHolder;
import com.example.smails.controller.exceptions.AllowedException;
import com.example.smails.destination.Destination;

import java.util.List;

/**
 * @author user
 */

public class DistanceService {

    private DistanceRepository distanceRepository = BeanHolder.getBean(DistanceRepository.class);



//    public Page<Distance> findAll(Pageable pageable) {
//        return distanceRepository.findAll(pageable);
//    }
//
    public List findAllRepresentations() {
        return distanceRepository.findAllRepresentations();
    }

    public void save(Distance distance) {
        distanceRepository.save(distance);
    }

//    public void deleteById(int from_id, int to_id) {
//        distanceRepository.deleteById(new DistanceId(from_id, to_id));
//    }
//
//    public Page<Distance> findAll(Pageable pageable, Optional<Integer> from_id, Optional<String> to_filter) {
//            return distanceRepository.findAll(pageable, from_id.orElse(-1), to_filter.orElse(""));
//    }
//
    public int getDistance(int from_id, int to_id) {
        //Distance distance = distanceRepository.findById(new DistanceId(from_id, to_id))
        Distance distance = distanceRepository.findBy("from_id", from_id, "to_id", to_id)
                .orElseThrow(() -> new AllowedException(String.format("Could not find distance between IDs %s and %s", from_id, to_id)));
        return distance.getValue();
    }

    public int getDistance(Destination from, Destination to) {
        try {
            return getDistance(from.getId(), to.getId());
        } catch (Exception ex){
            throw new AllowedException(String.format("Could not find distance between %s and %s", from, to), ex);
        }
    }
}
