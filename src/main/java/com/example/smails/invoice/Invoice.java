package com.example.smails.invoice;

import com.example.smails.account.Account;
import com.example.smails.controller.Document;
import com.example.smails.order.Order;

import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * @author user
 */

public class Invoice implements Document {


    private int id;

    Account account;

    private int number;

    private LocalDateTime created;

    private Order order;

    private BigDecimal total;

    private boolean paid;

    private String comment;

    public Invoice() {
    }

    public Invoice(Order order) {
        this.order = order;
        this.total = order.getTotal();
        this.comment = "Invoice of the order #" + order.getNumber();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public boolean getPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    //    public String getRepresentation() {
//        if ()
//        StringBuilder sb = new StringBuilder("Invoice ");
//        sb.append(number == 0 ? "*" : number);
//        sb.append(" from ");
//        sb.append(created == null ? '*' : created.toString());
//        return sb.toString();
//    }
}
