package com.example.smails.invoice;

import com.example.smails.account.Account;
import com.example.smails.controller.Document;
import com.example.smails.order.Order;
import com.example.smails.order.OrderForm;

import java.math.BigDecimal;
import java.time.LocalDateTime;


/**
 * @author user
 */

public class InvoiceForm implements Document {


    private int id;

    private int acc_id;

    private int number;

    private LocalDateTime created;

    private int order_id;

    private BigDecimal total;

//    private boolean paid;

    private String comment;

    public InvoiceForm() {
    }

    public InvoiceForm(OrderForm orderForm) {
        this.order_id = orderForm.getId();
        this.acc_id = orderForm.getAcc_id();
        this.total  = orderForm.getTotal();
        this.comment = "Invoice of the order #" + orderForm.getNumber();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAcc_id() {
        return acc_id;
    }

    public void setAcc_id(int acc_id) {
        this.acc_id = acc_id;
    }

    @Override
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

//    public boolean getPaid() {
//        return paid;
//    }
//
//    public void setPaid(boolean paid) {
//        this.paid = paid;
//    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
