package com.example.smails.invoice;

import com.example.smails.GenericRepository.GenericRepositoryImpl;
import com.example.smails.controller.DBCPDataSource;
import com.example.smails.controller.exceptions.DisallowedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author user
 */
public class InvoiceRepository extends GenericRepositoryImpl<Invoice, Integer> {
    private static final Logger log = LogManager.getLogger(InvoiceRepository.class);

    {
        init(Invoice.class);
    }

    public List<Map<String, Object>> findAllRepresentations() {
        return findAllRepresentations(-1);
    }

    public List<Map<String, Object>> findAllRepresentations(int account_id) {
        String sql = "SELECT i.*, acc.email account_email, o.number order_number, o.created order_created FROM "
                + table + " i\n"
                + "LEFT JOIN account acc ON acc.id = i.acc_id \n"
                + "LEFT JOIN orders o ON o.id = i.order_id \n";

        if (account_id > 0)
            return super.findAll(sql + "WHERE i.acc_id = ? \n", account_id);

        return super.findAll(sql);
    }

    public <S> Optional<S> findByIdAndAccount(Class<S> clazz, int id, int account_id) {
        return findBy(clazz, "id", id, "acc_id", account_id);
    }

    public void setPaidByIdAndAccount(int id, int account_id) {
        String sql = "update " + table + " set paid = true where id = ? and acc_id = ?";
        try (Connection con = DBCPDataSource.getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, id);
            ps.setInt(2, account_id);
            log.info("@@@@SQL: " + ps);
            if (ps.executeUpdate() != 1)
                throw new DisallowedException("Failed to set paid to Invoice ID " + id + ", account ID " + account_id);

        } catch (SQLException ex) {
            log.error("Failed to update table {}", table);
            throw new RuntimeException(ex);
        }
    }


//
//    public List<Map<String, Object>> findAllRepresentationsByAccount(Account account) {
//        return findAllRepresentations();
//    }
//
//    public Optional<Invoice> findByIdAndAccount(int id, Account account) {
//        return findBy("id", id, "acc_id", account.getId());
//    }

}
