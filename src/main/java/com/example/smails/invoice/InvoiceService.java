package com.example.smails.invoice;

import com.example.smails.account.Account;
import com.example.smails.account.Role;
import com.example.smails.controller.BeanHolder;
import com.example.smails.controller.EntityService;
import com.example.smails.controller.exceptions.DisallowedException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author user
 */
public class InvoiceService implements EntityService {

    private final InvoiceRepository invoiceRepository = BeanHolder.getBean(InvoiceRepository.class);

    public List<Map<String, Object>> findAll(Account account) {
        // only Manager is allowed to see all Orders
        if (account.hasRole(Role.ROLE_MANAGER))
            return invoiceRepository.findAllRepresentations();
        return invoiceRepository.findAllRepresentations(account.getId());
    }

    public InvoiceForm findById(int id, Account account) {
        return (account.hasRole(Role.ROLE_MANAGER) ?
                    invoiceRepository.findById(InvoiceForm.class, id) :
                    invoiceRepository.findByIdAndAccount(InvoiceForm.class, id, account.getId()))
                .orElseThrow(() -> new DisallowedException(
                        "No Invoice with id=" + id + " " + account.getEmail(), account));
    }

    public void save(InvoiceForm invoiceForm) {
        if (invoiceForm.getId() == 0) {
            invoiceForm.setCreated(LocalDateTime.now());
        }
        invoiceRepository.save(InvoiceForm.class, invoiceForm);
    }

    public void deleteById(int id) {
        invoiceRepository.deleteById(id);
    }

    public void payById(int id, Account account) {
        invoiceRepository.setPaidByIdAndAccount(id, account.getId());
    }

}
