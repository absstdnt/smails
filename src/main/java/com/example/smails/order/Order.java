package com.example.smails.order;

import com.example.smails.controller.Document;
import com.example.smails.destination.Destination;
import com.example.smails.account.Account;
import com.example.smails.orderType.OrderType;
//import com.example.smails.orderType.OrderType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;


/**
 * @author user
 */

//@Table(name = "orders")
public class Order implements Document {

    private Account account;
    private Destination from;
    private Destination to;
    private int id;
    private int number;
    private LocalDateTime created;
    private String address;
    private OrderType type;
    private int weight;
    private int length;
    private int width;
    private int height;
    private LocalDate deliveryDate;
    private BigDecimal total;

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Destination getFrom() {
        return from;
    }

    public void setFrom(Destination from) {
        this.from = from;
    }

    public Destination getTo() {
        return to;
    }

    public void setTo(Destination to) {
        this.to = to;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    //    public Order(OrderForm orderForm) {
//        this.id = orderForm.getId();
//        this.from = orderForm.getFrom();
//        this.to = orderForm.getTo();
//        this.address = orderForm.getAddress();
//        this.type = orderForm.getType();
//        this.weight = orderForm.getWeight();
//        this.length = orderForm.getLength();
//        this.width = orderForm.getWidth();
//        this.height = orderForm.getHeight();
//        this.deliveryDate = orderForm.getDeliveryDate();
//    }

    public String getDetails() {
        return new StringBuilder()
//                .append(type.getName()).append(' ')
                .append(deliveryDate).append(' ')
                .append(weight).append("kg ")
                .append(length).append("x")
                .append(width).append("x")
                .append(height).append("cm")
                .toString();
    }

}
