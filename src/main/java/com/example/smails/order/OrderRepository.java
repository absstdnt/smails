package com.example.smails.order;

import com.example.smails.GenericRepository.GenericRepositoryImpl;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author user
 */
public class OrderRepository extends GenericRepositoryImpl<Order, Integer> {
    {
        init(Order.class, "orders");
    }

    public List<Map<String, Object>> findAllRepresentations() {
        return findAllRepresentations(-1);
    }

    public List<Map<String, Object>> findAllRepresentations(int account_id) {
        String sql = "SELECT o.*, acc.email account_email, df.name from_name, dt.name to_name FROM "
                + table + " o\n"
                + "LEFT JOIN account acc ON o.acc_id = acc.id\n"
                + "LEFT JOIN destination df ON o.from_id = df.id\n"
                + "LEFT JOIN destination dt ON o.to_id = dt.id\n";

        if (account_id > 0)
            return super.findAll(sql + "WHERE o.acc_id = ? \n", account_id);

        return super.findAll(sql);
    }

    public Optional<Order> findByIdAndAccount(int id, int account_id) {
        return findBy("id", id, "acc_id", account_id);
    }

    public <S> Optional<S> findByIdAndAccount(Class<S> clazz, int id, int account_id) {
        return findBy(clazz, "id", id, "acc_id", account_id);
    }
}
