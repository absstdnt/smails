package com.example.smails.order;


import com.example.smails.controller.EntityService;
import com.example.smails.controller.BeanHolder;
import com.example.smails.controller.exceptions.DisallowedException;
import com.example.smails.account.Account;
import com.example.smails.account.Role;
import com.example.smails.distance.DistanceService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author user
 */
public class OrderService implements EntityService {

    private final OrderRepository orderRepository = BeanHolder.getBean(OrderRepository.class);
    private final DistanceService distanceService = BeanHolder.getBean(DistanceService.class);
    private final PriceService priceService = BeanHolder.getBean(PriceService.class);


    public List<Map<String, Object>> findAllRepresentations(Account account) {
        // only Manager is allowed to see all Orders
        if (account.hasRole(Role.ROLE_MANAGER))
            return orderRepository.findAllRepresentations();
        return orderRepository.findAllRepresentations(account.getId());
    }

    public OrderForm findById(int id, Account account) {
        // only Manager is allowed to see all Orders
        return (account.hasRole(Role.ROLE_MANAGER) ?
                orderRepository.findById(OrderForm.class, id) :
                orderRepository.findByIdAndAccount(OrderForm.class, id, account.getId()))
                .orElseThrow(() -> new DisallowedException(
                        "No Order with id=" + id + " " + account.getEmail(), account));
    }

    public OrderForm save(OrderForm orderForm, Account account) {
        // only Manager is allowed to Edit Orders
        if (orderForm.getId() > 0 && !account.hasRole(Role.ROLE_MANAGER))
            throw new DisallowedException(
                    "Attempt to edit order " + orderForm.getId() + " " + account.getEmail(), account);
        if (orderForm.getId() == 0) {
            orderForm.setCreated(LocalDateTime.now());
            orderForm.setAcc_id(account.getId());
        }
        orderForm.setTotal(getTotal(orderForm));
        return orderRepository.save(OrderForm.class, orderForm);
    }

        public BigDecimal getTotal(OrderForm orderForm) {
        BigDecimal volume = new BigDecimal(orderForm.getWidth() * orderForm.getLength() * orderForm.getHeight())
                .divide(new BigDecimal(1000), RoundingMode.HALF_UP);
        BigDecimal volumeWeight = volume.max(new BigDecimal(orderForm.getWeight()));
        int distance = distanceService.getDistance(orderForm.getFrom_id(), orderForm.getTo_id());
        return priceService.getPriceBase()
                .add(priceService.getPriceKmKg()
                        .multiply(new BigDecimal(distance))
                        .multiply(volumeWeight)).setScale(2, RoundingMode.HALF_UP);
    }

    public void deleteById(int id) {
        //todo
//        if (invoiceService.existsByOrderId(id))
//            throw new AllowedException("Order " + id + " has Invoices");
        orderRepository.deleteById(id);
    }

}
