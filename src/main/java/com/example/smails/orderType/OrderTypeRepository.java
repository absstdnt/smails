package com.example.smails.orderType;

import com.example.smails.GenericRepository.GenericRepositoryImpl;

/**
 * @author user
 */
public class OrderTypeRepository extends GenericRepositoryImpl<OrderType, Integer> {
    {
        init(OrderType.class, "order_type");
    }

}
