package com.example.smails.orderType;


import com.example.smails.controller.BeanHolder;
import com.example.smails.controller.EntityService;
import com.example.smails.controller.exceptions.AllowedException;

import java.util.List;

/**
 * @author user
 */
public class OrderTypeService implements EntityService {

    private final OrderTypeRepository orderTypeRepository = BeanHolder.getBean(OrderTypeRepository.class);

    public List<OrderType> findAll() {
        return orderTypeRepository.findAll();
    }

    public OrderType findById(int id) {
        return orderTypeRepository.findById(id).orElseThrow(() -> new AllowedException("No OrderType with id=" + id));
    }

    public void save(OrderType orderType) {
        orderTypeRepository.save(orderType);
    }

    public void deleteById(int id) {
        orderTypeRepository.deleteById(id);
    }
}
