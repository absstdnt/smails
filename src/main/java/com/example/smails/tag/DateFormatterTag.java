package com.example.smails.tag;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.SkipPageException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * @author user
 */
public class DateFormatterTag extends SimpleTagSupport {
    private static final Logger log = LogManager.getLogger(DateFormatterTag.class);
    private DateTimeFormatter formatter;
    StringWriter sw = new StringWriter();

    public DateFormatterTag() {
        //formatter = DateTimeFormatter.ofPattern(messageSource.getMessage("representation.dateTime.template", null, locale), locale);
        formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    }

    public void setFormat(String format) {
        formatter = DateTimeFormatter.ofPattern(format);
    }

    @Override
    public void doTag() throws JspException, IOException {
        getJspBody().invoke(sw);
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String date = sw.toString().substring(0, inputPattern.length());
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern(inputPattern);
        LocalDateTime localDateTime = LocalDateTime.parse(date, inputFormatter);
        try {
            getJspContext().getOut().write(localDateTime.format(formatter));
        } catch (Exception e) {
            String message = "Exception in formatting " + date
                    + " with format " + formatter;
            log.error(message, e);
            throw new SkipPageException(message);
        }
    }

}
