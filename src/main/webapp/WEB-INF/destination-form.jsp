<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="/imports.jsp"/>
</head>
<body>

<jsp:include page="/header.jsp"/>

<header class="header-regular">
    <div class="container">
        <div class="row row-header">
                <h1><fmt:message key="entity.destinations" /></h1>
        </div>
    </div>
</header>

<div class="main-container">

    <form method="post" action="${pageContext.request.contextPath}/destination/save">
        <input type="hidden" name="id" value="${destination.id}">

        <div class="form-group row">
            <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="general.name" /></label>
            <div class="form-select">
                <input class="form-control ${errors.name != null ? 'is-invalid' : ''}"
                    name="name" value="${destination.name}"/>
                <c:if test="${errors.name != null}">
                    <div class="invalid-feedback" th:errors="name">${errors.name}</div>
                </c:if>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-form-label text-truncate"></label>
            <div>
                <button type="submit" class="btn btn-primary"><fmt:message key="general.save" /></button>
                <a class="btn btn-secondary"  href="."><fmt:message key="general.cancel" /></a>
            </div>
        </div>
    </form>

</div>

<jsp:include page="/footer.jsp" />

</body>
</html>
