<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="/imports.jsp"/>
</head>
<body>

<jsp:include page="/header.jsp"/>

<header class="header-regular">
    <div class="container">
        <div class="row row-header">
                <h1><fmt:message key="entity.destinations" /></h1>
        </div>
    </div>
</header>

<div class="main-container">
<!-- Add button -->
<div>
    <a href="${pageContext.request.contextPath}/destination/add"
       class="btn btn-primary"><fmt:message key="general.add" /></a>
</div>
<br>
    <table class="table table-bordered">
        <thead class="thead-light">
          <tr>
            <th>#</th>
            <th width='70%'><fmt:message key="general.name" /></th>
            <th><fmt:message key="general.action" /></th>
          </tr>
        </thead>
        <tbody>
          <c:forEach items="${list}" var="item">
            <tr>
              <td><c:out value="${item.id}" /></td>
              <td><c:out value="${item.name}" /></td>
              <td>
                <a class="btn btn-secondary btn-sm"
                   href="${pageContext.request.contextPath}/destination/edit?id=${item.id}"><fmt:message key="general.edit" /></a>
                <a class="btn btn-danger btn-sm"
                   onclick="if (!(confirm('Are you sure you want to delete this item?'))) return false"
                   href="${pageContext.request.contextPath}/destination/delete?id=${item.id}"><fmt:message key="general.delete" /></a>
              </td>
            </tr>
          </c:forEach>
        </tbody>
    </table>

</div>


<jsp:include page="/footer.jsp" />

</body>
</html>
