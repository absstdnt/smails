<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="/imports.jsp"/>
</head>
<body>
<jsp:include page="/header.jsp"/>
<header class="header-regular">
    <div class="container">
        <div class="row row-header">
                <h1><fmt:message key="entity.distances" /></h1>
        </div>
    </div>
</header>

<div class="main-container">
<br>
    <table class="table table-bordered">
        <thead class="thead-light">
          <tr>
            <th><fmt:message key="entity.distance.from" /></th>
            <th><fmt:message key="entity.distance.to" /></th>
            <th><fmt:message key="entity.distance.value" /></th>
          </tr>
        </thead>
        <tbody>
          <c:forEach items="${list}" var="item">
            <tr>
              <td>${item.from_name}</td>
              <td>${item.to_name}</td>
              <td>${item.value}</td>
            </tr>
          </c:forEach>
        </tbody>
    </table>
</div>

<jsp:include page="/footer.jsp" />

</body>
</html>
