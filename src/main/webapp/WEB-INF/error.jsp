<%@ page language="java" isErrorPage="true"  contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="robots" content="noindex">

    <title>Smail</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/bs.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/style.css" />

</head>
    <body>
        <div class="container">
            <h1>Please try again later</h1>
            <p>${description}</p>
            <p><%= exception %></p>
            <a href="${pageContext.request.contextPath}/">Back to Home Page</a>
        </div>
    </body>
</html>
