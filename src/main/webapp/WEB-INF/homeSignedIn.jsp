<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="/imports.jsp"/>
</head>
<body>
    <jsp:include page="/header.jsp"/>
    <header class="jumbotron">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 col-sm-6">
                    <h1><fmt:message key="home.title" /></h1>
                    <p><fmt:message key="home.motto" /></p>
                </div>
            </div>
        </div>
    </header>

        <div class="main-container">
            <div class="info-container">
                <h2>Доставка вантажу</h2>
                <p>Існують ролі: Користувач, Авторизований користувач, Менеджер.
                    На сайті компанії доставки вантажів розміщено інформацію про напрямки доставки, а також про тарифи (тарифи залежать від відстані, ваги та габаритів вантажу).
                    Незареєстрований користувач може переглядати інформацію на сайті, відсортувавши чи відфільтрувавши її за напрямами доставки, а також має можливість самостійно розрахувати вартість послуг.
                    Авторизований користувач може створити заявку на доставку вантажу і вказати адресу доставки. Заявка містить інформацію про тип багажу, вагу, об’єм і дату отримання.
                    Менеджер опрацьовує заявки і формує квитанції до сплати, а також може отримувати звіти щодо доставок (по днях та напрямках).
                    Авторизований користувач в своєму кабінеті може сплатити квитанцію на доставку.</p>
            </div>
            <a href="http://fp-smail.herokuapp.com">Counterpart</a>
        </div>

    <jsp:include page="/footer.jsp" />

</body>
</html>
