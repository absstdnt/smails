<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="/imports.jsp"/>
</head>
<body>

<jsp:include page="/header.jsp"/>

<header class="header-regular">
    <div class="container">
        <div class="row row-header">
                <h1><fmt:message key="entity.invoice" /></h1>
        </div>
    </div>
</header>

<div class="container main-container">

    <form method="post" action="${pageContext.request.contextPath}/invoice/save">
        <input type="hidden" name="id" value="${invoice.id}">
        <input type="hidden" name="acc_id" value="${invoice.acc_id}">
        <input type="hidden" name="order_id" value="${invoice.order_id}">

        <div class="form-group row">
            <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="general.comment" /></label>
            <div class="form-select">
                <input class="form-control ${errors.comment != null ? 'is-invalid' : ''}"
                        name="comment" value="${invoice.comment}">
                <c:if test="${errors.comment != null}">
                    <div class="invalid-feedback">${errors.comment}</div>
                </c:if>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="entity.invoice.total" /></label>
            <div>
                <input type="number" step=".01" class="form-control ${errors.total != null ? 'is-invalid' : ''}"
                        name="total" value="${invoice.total}">
                <c:if test="${errors.total != null}">
                    <div class="invalid-feedback">${errors.total}</div>
                </c:if>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-form-label text-truncate"></label>
            <div class="offset-lg-3 col-lg-9">

                <c:if test="${invoice == null || invoice.id == 0}">
                    <button type="submit" class="btn btn-primary"
                            sec:authorize="hasAnyRole('MANAGER', 'AUSER')">
                            <fmt:message key="general.save" /></button>
                </c:if>

                <c:if test="${invoice != null && invoice.id != 0}">
                    <button type="submit" class="btn btn-primary"
                            sec:authorize="hasRole('MANAGER')"><fmt:message key="general.update" /></button>
                </c:if>

                <a class="btn btn-secondary" href="."><fmt:message key="general.cancel" /></a>
            </div>
        </div>
    </form>

</div>

<jsp:include page="/footer.jsp" />

</body>
</html>
