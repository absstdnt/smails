<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="mytags" uri="https://fp-smails.herokuapp.com/tlds/mytags" %>


<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="/imports.jsp"/>
</head>
<body>

<jsp:include page="/header.jsp"/>

<header class="header-regular">
    <div class="container">
        <div class="row row-header">
                <h1><fmt:message key="entity.invoices" /></h1>
        </div>
    </div>
</header>

<div class="main-container">

<br>
    <table class="table table-bordered">
        <thead class="thead-light">
          <tr>
            <th style="width: 8%"><fmt:message key="general.number" /></th>
            <th style="width: 10%"><fmt:message key="general.created" /></th>
            <th><fmt:message key="entity.account" /></th>
            <th><fmt:message key="entity.order" /></th>
            <th><fmt:message key="entity.invoice.total" /></th>
            <th><fmt:message key="general.comment" /></th>
            <th><fmt:message key="entity.invoice.paid" /></th>
            <th><fmt:message key="general.action" /></th>
          </tr>
        </thead>
        <tbody>
          <fmt:message key="representation.dateTime.template" var="dateTimeFormat" />

          <c:forEach items="${list}" var="item">
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/invoice/edit?id=${item.id}"><c:out value="${item.number}" /></a>
                </td>
                <td><mytags:formatDate format="${dateTimeFormat}">${item.created}</mytags:formatDate></td>
                <td>${item.account_email}</td>
                <td>${item.order_number}</td>
                <td>${item.total}</td>
                <td>${item.comment}</td>
                <td>${item.paid}</td>
                <td>
                    <c:if test="${ account.hasAnyRole('MANAGER') }">
                        <a class="btn btn-secondary btn-sm"
                           href="${pageContext.request.contextPath}/invoice/edit?id=${item.id}"><fmt:message key="general.edit" /></a>
                        <a class="btn btn-danger btn-sm"
                           onclick="if (!(confirm('Are you sure you want to delete this item?'))) return false"
                           href="${pageContext.request.contextPath}/invoice/delete?id=${item.id}"><fmt:message key="general.delete" /></a>
                    </c:if>
                    <c:if test="${ account.hasAnyRole('AUSER') }">
                    <a class="btn btn-danger btn-sm"
                       onclick="if (!(confirm('Are you sure you want to pay?'))) return false"
                       href="${pageContext.request.contextPath}/invoice/pay?id=${item.id}"><fmt:message key="general.pay" /></a>
                    </c:if>

                </td>
            </tr>
          </c:forEach>
        </tbody>
    </table>
</div>

<jsp:include page="/footer.jsp" />

</body>
</html>
