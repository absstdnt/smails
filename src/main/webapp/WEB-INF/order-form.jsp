<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="/imports.jsp"/>
    <script src="${pageContext.request.contextPath}/static/js/jquery.min.js"></script>

    <script>
    $(document).on("click", "#calculate", function(){
        $.post("${pageContext.request.contextPath}/calculate",
        $("#orderForm").serializeArray(),
        function(data,status){
          $("#total").val(data);
        });
    });
    </script>

</head>
<body>

<jsp:include page="/header.jsp"/>

<header class="header-regular">
    <div class="container">
        <div class="row row-header">
                <h1><fmt:message key="entity.order" /></h1>
        </div>
    </div>
</header>

<div class="container main-container">

    <form method="post" id="orderForm" action="${pageContext.request.contextPath}/order/save">
        <input type="hidden" name="id" value="${order.id}">

        <div class="form-group row">
            <label class="col-form-label text-truncate"><fmt:message key="entity.order.from" /></label>
            <div class="form-select">
                <select class="form-control ${errors.from_id != null ? 'is-invalid' : ''}"
                        name="from_id" value="${order.from_id}">
                     <option value="-1"></option>
                     <c:forEach items="${destinations}" var="destination">
                         <option value="${destination.id}" ${destination.id == order.from_id ? 'selected' : ''}>${destination.name}</option>
                     </c:forEach>
                </select>
                <c:if test="${errors.from_id != null}">
                    <div class="invalid-feedback">${errors.from_id}</div>
                </c:if>
            </div>
        </div>
        
        <div class="form-group row">
            <label class="col-form-label text-truncate"><fmt:message key="entity.order.to" /></label>
            <div class="form-select">
                <select class="form-control ${errors.to_id != null ? 'is-invalid' : ''}"
                        name="to_id" value="${order.to_id}">
                     <option value="-1"></option>
                     <c:forEach items="${destinations}" var="destination">
                         <option value="${destination.id}" ${destination.id == order.to_id ? 'selected' : ''}>${destination.name}</option>
                     </c:forEach>
                </select>
                <c:if test="${errors.to_id != null}">
                    <div class="invalid-feedback">${errors.to_id}</div>
                </c:if>
            </div>
        </div>

        <c:if test="${account.isAuthenticated()}">
            <div class="form-group row">
                <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="entity.order.address" /></label>
                <div class="form-select">
                    <input class="form-control ${errors.address != null ? 'is-invalid' : ''}"
                            name="address" value="${order.address}">
                    <c:if test="${errors.address != null}">
                        <div class="invalid-feedback">${errors.address}</div>
                    </c:if>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label text-truncate"><fmt:message key="entity.order.type" /></label>
                <div class="form-select">
                    <select class="form-control ${errors.type_id != null ? 'is-invalid' : ''}"
                            name="type_id" value="${order.type_id}">
                         <option value="-1"></option>
                         <c:forEach items="${orderTypes}" var="orderType">
                             <option value="${orderType.id}" ${orderType.id == order.type_id ? 'selected' : ''}>${orderType.name}</option>
                         </c:forEach>
                    </select>
                    <c:if test="${errors.type_id != null}">
                        <div class="invalid-feedback">${errors.type_id}</div>
                    </c:if>
                </div>
            </div>
        </c:if>

        <div class="form-group row">
            <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="entity.order.weight" /></label>
            <div>
                <input type="number" class="form-control ${errors.weight != null ? 'is-invalid' : ''}"
                        name="weight" value="${order.weight}">
                <c:if test="${errors.weight != null}">
                    <div class="invalid-feedback">${errors.weight}</div>
                </c:if>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="entity.order.dimensions" /></label>

                <div>
                <input type="number" class="form-control ${errors.length != null ? 'is-invalid' : ''}"
                        name="length" value="${order.length}">
                </div>
                <c:if test="${errors.length != null}">
                    <div class="invalid-feedback">${errors.length}</div>
                </c:if>
                <label class="col-form-label-delimiter">x</label>
                <div>
                <input type="number" class="form-control ${errors.width != null ? 'is-invalid' : ''}"
                        name="width" value="${order.width}">
                </div>
                <c:if test="${errors.width != null}">
                    <div class="invalid-feedback">${errors.width}</div>
                </c:if>
                <label class="col-form-label-delimiter">x</label>
                <div>
                <input type="number" class="form-control ${errors.height != null ? 'is-invalid' : ''}"
                        name="height" value="${order.height}">
                </div>

            <c:if test="${errors.height != null}">
                <div class="invalid-feedback">${errors.height}</div>
            </c:if>
            
        </div>       

        <c:if test="${account.isAuthenticated()}">

            <div class="form-group row">
                <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="entity.order.deliveryDate" /></label>
                <div>
                    <input type="date" class="form-control ${errors.deliveryDate != null ? 'is-invalid' : ''}"
                            name="deliveryDate" value="${order.deliveryDate}">
                    <c:if test="${errors.deliveryDate != null}">
                        <div class="invalid-feedback">${errors.deliveryDate}</div>
                    </c:if>
                </div>
            </div>

        </c:if>

        <div class="form-group row">
            <label class="col-lg-3 col-form-label text-truncate"><fmt:message key="entity.order.total" /></label>
            <div>
                <input type="number"  class="form-control ${errors.total != null ? 'is-invalid' : ''}"
                        id="total" name="total" value="${order.total}" readonly>
                <c:if test="${errors.total != null}">
                    <div class="invalid-feedback">${errors.total}</div>
                </c:if>
            </div>&nbsp;&nbsp;
            <div class="col-lg-3">
                <button type="button" class="btn btn-info" id="calculate">
                    <fmt:message key="general.calculate" />
                </button>
            </div>
        </div>


        <div class="form-group row">
            <label class="col-form-label text-truncate"></label>
            <div class="offset-lg-3 col-lg-9">

                <c:if test="${ account.hasAnyRole('MANAGER', 'AUSER') }">

                    <c:if test="${order == null || order.id == 0}">
                        <button type="submit" class="btn btn-primary">
                                <fmt:message key="general.save" /></button>
                    </c:if>

                    <c:if test="${order != null && order.id != 0 && account.hasAnyRole('MANAGER')}">
                        <button type="submit" class="btn btn-primary"><fmt:message key="general.update" /></button>
                    </c:if>

                </c:if>

                <a class="btn btn-secondary" href="."><fmt:message key="general.cancel" /></a>
            </div>
        </div>
    </form>

</div>

<jsp:include page="/footer.jsp" />

</body>
</html>
