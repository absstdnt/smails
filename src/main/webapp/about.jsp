
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="imports.jsp"/>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <header class="jumbotron">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 col-sm-6">
                    <h1>Sm@il</h1>
                    <p>Not so fast!</p>
                </div>
            </div>
        </div>
    </header>

        <div class="main-container">
            <div class="info-container">
                <h2>Доставка вантажу</h2>
                <p>Існують ролі: Користувач, Авторизований користувач, Менеджер.
                    На сайті компанії доставки вантажів розміщено інформацію про напрямки доставки, а також про тарифи (тарифи залежать від відстані, ваги та габаритів вантажу).
                    Незареєстрований користувач може переглядати інформацію на сайті, відсортувавши чи відфільтрувавши її за напрямами доставки, а також має можливість самостійно розрахувати вартість послуг.
                    Авторизований користувач може створити заявку на доставку вантажу і вказати адресу доставки. Заявка містить інформацію про тип багажу, вагу, об’єм і дату отримання.
                    Менеджер опрацьовує заявки і формує квитанції до сплати, а також може отримувати звіти щодо доставок (по днях та напрямках).
                    Авторизований користувач в своєму кабінеті може сплатити квитанцію на доставку.</p>
            </div>
        </div>

    <jsp:include page="footer.jsp" />

</body>
</html>
