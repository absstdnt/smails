<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>


<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

    <div class="topnav" id="myTopnav">
      <div class="topnav-left">
          <a class="active" href="${pageContext.request.contextPath}/"><fmt:message key="header.home" /></a>
          <a href="${pageContext.request.contextPath}/about"><fmt:message key="header.about" /></a>
          <a href="${pageContext.request.contextPath}/distance"><fmt:message key="entity.distances" /></a>
          <c:if test="${!account.isAuthenticated()}">
              <a href="${pageContext.request.contextPath}/calculate"><fmt:message key="header.calculate" /></a>
          </c:if>
          <c:if test="${ account.hasAnyRole('MANAGER', 'AUSER') }">
              <a href="${pageContext.request.contextPath}/order"><fmt:message key="entity.orders" /></a>
              <a href="${pageContext.request.contextPath}/invoice"><fmt:message key="entity.invoices" /></a>
              <c:if test="${ account.hasAnyRole('MANAGER') }">
                  <a href="${pageContext.request.contextPath}/destination"><fmt:message key="entity.destinations" /></a>
                  <%-- <a href="${pageContext.request.contextPath}/report/order"><fmt:message key="header.reports" /></a>  --%>
              </c:if>
          </c:if>
      </div>
      <div class="topnav-right">

          <c:set var = "langParam" value = "${'lang='.concat(sessionScope.lang)}" />
          <c:set var = "paramsString" value = "${fn:replace(fn:replace(pageContext.request.queryString, '&'.concat(langParam), ''), langParam, '')}" />

          <a href="?${paramsString}&lang=en">en</a>
          <a href="?${paramsString}&lang=ua">ua</a>
          <c:if test="${!account.isAuthenticated()}">
              <a href="${pageContext.request.contextPath}/login">login</a>
          </c:if>

          <c:if test="${account.isAuthenticated()}">
              <a href="${pageContext.request.contextPath}/logout">logout ${account}</a>
          </c:if>
          <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <i class="fa fa-bars"></i>
          </a>
      </div>
    </div>
