
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page session="true" %>

<fmt:requestEncoding value="UTF-8" />
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="messages"/>

<html lang="${param.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="imports.jsp"/>
</head>
<body>
    <jsp:include page="header.jsp"/>
    <header class="jumbotron">
        <div class="container">
            <div class="row row-header">
                <div class="col-12 col-sm-6">
                    <h1><fmt:message key="home.title" /></h1>
                    <p><fmt:message key="home.motto" /></p>
                </div>
            </div>
        </div>
    </header>

        <div class="main-container">
            <div class="info-container">

                    <h2>Welcome to Sm@il!</h2>
                    <p>Please register to take all benefits of our service.</p>
                    <a href="https://fp-smail.herokuapp.com/signup" id="signup" class="btn btn-primary">Sign up</a>

            </div>
        </div>

    <jsp:include page="footer.jsp" />

</body>
</html>
