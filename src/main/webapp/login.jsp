<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <jsp:include page="imports.jsp" />
</head>
<body>
    <jsp:include page="header.jsp" />

        <div class="main-container">

            <form class="form-narrow container" method="post" action="${pageContext.request.contextPath}/login">

                <div style="color: #dc3545"><h6>${message}</h6></div>
                <label for="name"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="name" value="${name}" required>

                <label for="pass"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="pass" required>

                <button type="submit" class="btn btn-primary btn-long">Login</button>

            </form>

        </div>

    <jsp:include page="footer.jsp" />

</body>
</html>