<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, java.text.*" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SmailS</title>
    <jsp:include page="imports.jsp"/>
</head>
<body>
    <div class="container">
        <h1>Please try again later</h1>
        <p>${description}</p>
        <a href="${pageContext.request.contextPath}/">Back to Home Page</a>
    </div>
</body>
</html>