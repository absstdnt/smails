package com.example.smails;

import com.example.smails.account.Account;
import com.example.smails.controller.filters.AuthFilter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * @author user
 */
public class AuthFilterUserTest {
    private static final Logger log = LogManager.getLogger(AuthFilterUserTest.class);

    AuthFilter authFilter = new AuthFilter();

    HttpServletRequest req = Mockito.mock(HttpServletRequest.class);
    RequestDispatcher requestDispatcher = Mockito.mock(RequestDispatcher.class);
    HttpServletResponse res = Mockito.mock(HttpServletResponse.class);
    FilterChain filterChain = Mockito.mock(FilterChain.class);
    HttpSession httpSession = Mockito.mock(HttpSession.class);
    Account account = Mockito.mock(Account.class);


    @Before
    public void setUp() throws Exception {
        Mockito.when(req.getSession()).thenReturn(httpSession);
        Mockito.when(req.getSession().getAttribute("account")).thenReturn(account);
        Mockito.when(req.getRequestDispatcher("/login.jsp")).thenReturn(requestDispatcher);
        //Mockito.when(account.hasRole(Role.ROLE_AUSER)).thenReturn(true);

        FilterConfig mockFilterConfig = Mockito.mock(FilterConfig.class);
        authFilter.init(mockFilterConfig);
    }

    @After
    public void tearDown() throws Exception {
        authFilter.destroy();
    }

    public void doFilter(String path) throws IOException, ServletException {
        doFilter(path, false);
    }

    public void doFilter(String path, boolean forbidden) throws IOException, ServletException {

        Mockito.when(req.getServletPath()).thenReturn(path);

        authFilter.doFilter(req, res, filterChain);
        if (forbidden) verify(res).setStatus(403);
        else verify(res, never()).setStatus(403);

    }

    // */
    @Test
    public void home() throws Exception {
        doFilter("/");
    }

    // *about
    @Test
    public void about() throws Exception {
        doFilter("/about");
    }

    // *login
    @Test
    public void login() throws Exception {
        doFilter("/login");
    }

    @Test
    public void order() throws Exception {
        doFilter("/order", true);
    }

    @Test
    public void order_add() throws Exception {
        doFilter("/order/add", true);
    }

    @Test
    public void order_edit() throws Exception {
        doFilter("/order/edit", true);
    }

    @Test
    public void order_save() throws Exception {
        doFilter("/order/save", true);
    }

    @Test
    public void order_delete() throws Exception {
        doFilter("/order/delete", true);
    }

    @Test
    public void invoice() throws Exception {
        doFilter("/invoice", true);
    }

    @Test
    public void invoice_add() throws Exception {
        doFilter("/invoice/add", true);
    }

    @Test
    public void invoice_save() throws Exception {
        doFilter("/invoice/save", true);
    }

    @Test
    public void invoice_edit() throws Exception {
        doFilter("/invoice/edit", true);
    }

    @Test
    public void invoice_delete() throws Exception {
        doFilter("/invoice/delete", true);
    }

    @Test
    public void invoice_pay() throws Exception {
        doFilter("/invoice/pay", true);
    }

    //distance
    @Test
    public void distance() throws Exception {
        doFilter("/distance");
    }

    //destination
    @Test
    public void destination() throws Exception {
        doFilter("/destination", true);
    }


}